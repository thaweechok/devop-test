# Dev ops Test

## Ansible Exam.

1. Create Directory '/mnt/devops'. If it exists already just report that the directory has already exist. 
2. Download file from https://www.dropbox.com/s/y235utxv7s1cmq1/test.txt?dl=1 and put into /mnt/devops folder.
3. Check md5 of downlowned file against https://www.dropbox.com/s/qhz3bgiw1x3nmmb/test.md5?dl=1